/*
 * Entidad Lista de Profesor
 */
package profesmar;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author mfontana
 */
public class ListaProfesores implements Serializable {

    private ArrayList<Profesor> lista;

    public ListaProfesores() {
        lista = new ArrayList<>();
    }
    
    public void altaProfesor(Profesor p) {
        lista.add(p);
    }
    
    public void bajaProfesor(Profesor p) {
        lista.remove(p);
    }
    
    public boolean existe(Profesor p) {
        return lista.contains(p);
    }
    
    // Método que devuelve la lista de especialidades
    // de los profesores que hay en la lista (sin repetidos)
    public ArrayList<String> especialidades() {
        ArrayList<String> esp = new ArrayList<>();
        for (Profesor p : lista) {
            if (!esp.contains(p.getEspecialidad())) {
                esp.add(p.getEspecialidad());
            }
        }
        return esp;
    }
    
    // Método que devuelve los profesores de una especialidad determinada
    public ListaProfesores profesoresPorEspecialidad(String especialidad) {
        ListaProfesores profesEsp = new ListaProfesores();
        for (Profesor p : lista) {
            if (especialidad.equalsIgnoreCase(p.getEspecialidad())) {
                profesEsp.altaProfesor(p);
            }
        }
        return profesEsp;
    }
    
    public ArrayList<Profesor> getLista() {
        return lista;
    }

    public void setLista(ArrayList<Profesor> lista) {
        this.lista = lista;
    }

}
